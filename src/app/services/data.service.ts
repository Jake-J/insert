import { TableData } from './../models/TableData';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ChartsData, ChartsRecordTemplate } from '../models/ChartsData';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getCSV(csvName: string): Observable<any>{
    return this.http.get(`./assets/data/${csvName}.csv`, {responseType: 'text'});
  }

  getMappedData(dataName): Observable<any> {
    return this.getCSV(dataName).pipe(
      map( response => {
        const jsonData =  this.csvJSON(response);
        const chartsData = this.prepareChartData(jsonData);
        const tableData = this.prepareTableData(jsonData);

        return {
          chartsData,
          tableData
        };
      }),
      catchError( error => {
        return throwError(error); // From 'rxjs'
    })
    );
  }

  prepareChartData(data): ChartsData {
    const recordKeys = Object.keys(data[0]);
    const records: ChartsRecordTemplate[] = [];
    let categories: string[] = [];
    for (const key of recordKeys) {
      if (key !== 'X') {
        records.push({
          name: key,
          values: data.reduce((acc, curr, idx) => (acc[idx] = curr[key].replace(',', '.').replace(/[`~!@#$%^&*()_|+=?;:'",<>\{\}\[\]\\\/]/gi, ''), acc), [])
        });
      } else {
        categories = data.reduce((acc, curr, idx) => (acc[idx] = curr[key].replace(',', '.').replace(/[`~!@#$%^&*()_|+=?;:'",<>\{\}\[\]\\\/]/gi, ''), acc), []);
      }
    }

    return {
      categories,
      records,
    };
  }

  prepareTableData(data): TableData {
    const tableHeaders = Object.keys(data[0]);

    return {
      headers: tableHeaders,
      originalHeaders: tableHeaders,
      records: data
    };
  }

  // @todo: func return type change
  csvJSON(csv): any[] {
    const lines = csv.split('\n');
    const result = [];
    const headers = lines[0].split(';');

    for (let i = 1; i < lines.length; i++) {
        if (!lines[i]) {
            continue;
        }
        const obj = {};
        const currentline = lines[i].split(';');

        for (let j = 0; j < headers.length; j++) {
            obj[headers[j].trim()] = currentline[j];
        }
        result.push(obj);
    }
    return result;
  }
}
