import { SiteTemplate } from './../../models/SiteTemplate';
import { DataService } from './../../services/data.service';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { tap } from 'rxjs/internal/operators/tap';
@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.scss']
})
export class SiteComponent implements OnInit, AfterViewInit, OnDestroy {

  // siteTemplate: (SiteTemplate);
  siteTemplate$: any;

  isLoading = true;
  fragment: string = null;
  subs: Subscription[] = [];
  siteDataType: any = null;

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.siteDataType = this.route.snapshot.data.type;
    this.siteTemplate$ = this.dataService.getMappedData(`data${this.siteDataType}`).pipe(
      map(_data => ({..._data, dataType: this.siteDataType })),
    );
  }

  ngAfterViewInit(): void {
    this.subs.push(this.route.fragment.subscribe(fragment => {
      this.fragment = fragment;
      setTimeout(() => this.scrollToAnchor(), 200);
    }));
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe);
  }

  scrollToAnchor(): void {
    try {
      if (this.fragment) {
        document.querySelector('#' + this.fragment).scrollIntoView(({behavior: 'smooth'}));
      }
    } catch (e) { }
  }

  applyFilters(): void {
    const queryParams = this.route.snapshot.queryParams;
    this.siteTemplate$ = this.siteTemplate$.pipe(tap((parsedData: any) => {
      parsedData.tableData.headers = queryParams.columns
        ? parsedData.tableData.originalHeaders.filter(header => queryParams.columns.includes(header) || header === 'X')
        : parsedData.tableData.originalHeaders;

      this.scrollToAnchor();
    }));
  }
}
