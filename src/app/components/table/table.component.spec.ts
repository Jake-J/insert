import { compileComponentFromMetadata } from '@angular/compiler';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';

// table data mock:

const tableData = {
  headers: [
      'X',
      'Y1',
      'Y2',
      'Y3',
      'Y4',
      'Y5'
  ],
  originalHeaders: [
      'X',
      'Y1',
      'Y2',
      'Y3',
      'Y4',
      'Y5'
  ],
  records: [
      {
          X: 'styczeń',
          Y1: '0,999275846',
          Y2: '3100,35888',
          Y3: '0,059087424',
          Y4: '274719,1178',
          Y5: '0,004153475'
      },
      {
          X: 'luty',
          Y1: '0,824858801',
          Y2: '5356,141055',
          Y3: '0,102078691',
          Y4: '219062,1757',
          Y5: '0,006762655'
      },
      {
          X: 'marzec',
          Y1: '0,304004652',
          Y2: '891,8945225',
          Y3: '0,016997951',
          Y4: '491645,4205',
          Y5: '0,004670127'
      },
      {
          X: 'kwiecień',
          Y1: '0,921498027',
          Y2: '937,6290834',
          Y3: '0,017869572',
          Y4: '409905,1411',
          Y5: '0,000255832'
      },
      {
          X: 'maj',
          Y1: '0,776070889',
          Y2: '3903,300529',
          Y3: '0,074390089',
          Y4: '86539,2909',
          Y5: '0,008276929'
      },
      {
          X: 'czerwiec',
          Y1: '0,523913776',
          Y2: '3835,437046',
          Y3: '0,073096729',
          Y4: '464659,3977',
          Y5: '0,004474329'
      },
      {
          X: 'lipiec',
          Y1: '0,906331608',
          Y2: '5463,021846',
          Y3: '0,104115652',
          Y4: '998741,6458',
          Y5: '0,003858839'
      },
      {
          X: 'sierpień',
          Y1: '0,417367291',
          Y2: '2695,071728',
          Y3: '0,051363359',
          Y4: '977408,8111',
          Y5: '0,005750201'
      },
      {
          X: 'wrzesień',
          Y1: '0,723426979',
          Y2: '8858,804128',
          Y3: '0,16883333',
          Y4: '859686,08',
          Y5: '0,003728438'
      },
      {
          X: 'październik',
          Y1: '0,477686748',
          Y2: '8726,563793',
          Y3: '0,16631306',
          Y4: '950088,1717',
          Y5: '0,005984764'
      },
      {
          X: 'listopad',
          Y1: '0,652762058',
          Y2: '3969,335795',
          Y3: '0,075648606',
          Y4: '32523,28129',
          Y5: '0,008697549'
      },
      {
          X: 'grudzień',
          Y1: '0,090059638',
          Y2: '4733,148263',
          Y3: '0,090205537',
          Y4: '616583,0898',
          Y5: '0,002433191'
      }
  ]
};

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    component.tableData = tableData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


