import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  fragment$: any = null;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.fragment$ = this.route.fragment;
  }
}
