import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTooltip,
  ApexStroke,
  ApexFill,
  ApexYAxis,
  ApexMarkers,
  ApexGrid
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  stroke: ApexStroke;
  tooltip: ApexTooltip;
  dataLabels: ApexDataLabels;
  fill: ApexFill;
  markers: ApexMarkers;
  grid: ApexGrid;
};

@Component({
  selector: 'app-linear-chart',
  templateUrl: './linear-chart.component.html',
  styleUrls: ['./linear-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinearChartComponent implements OnInit {
  @ViewChild('chart') chart: ChartComponent;
  @Input() categories: any;
  @Input() chartData: any;
  public chartOptions: Partial<ChartOptions>;

  constructor() { }

  ngOnInit(): void {
    this.chartOptions = {
      series: [
        {
          name: 'series1',
          data: this.chartData.values,
          color: '#54D8FF'
        },
      ],
      chart: {
        height: 350,
        type: 'area',
        stacked: false,
        toolbar: {
          show: false,
        }
      },
      fill: {
        type: 'gradient',
        gradient: {
          shadeIntensity: 1,
          opacityFrom: 0.7,
          opacityTo: 0.9,
          stops: [0, 60, 75],
        }
      },
      markers: {
        size: 3,
        colors: ['#fff'],
        strokeColors: '#54D8FF',
        strokeWidth: 1,
        hover: {
          size: 5,
        }
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'smooth',
        width: 1,
      },
      yaxis: {
        tickAmount: 6,
        forceNiceScale: false,
        decimalsInFloat: Math.max(...this.chartData.values.map(val => parseFloat(val)).filter(_val => !isNaN(_val))) < 1 ? 2 : 0,
      },
      xaxis: {
        type: 'category',
        categories: this.categories,
      },
      tooltip: {
        y: {
          formatter: (seriesName) => seriesName.toString(),
        }
      },
      grid: {
        position: 'front',
        yaxis: {
          lines: {
              show: true
          }
        },
        xaxis: {
          lines: {
              show: true
          }
        },
      },

    };
  }
}
