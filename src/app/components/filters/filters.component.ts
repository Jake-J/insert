import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FiltersTemplateList } from 'src/app/models/FiltersTemplate';
@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class FiltersComponent implements OnInit {
  @Output() filtersChanged = new EventEmitter<any>();

  filtersTemplate: FiltersTemplateList = {
    tableColumns: {
      label: 'Wybierz kolumny do wizualizacji',
      options: [
        {
          name: 'Y1'
        },
        {
          name: 'Y2'
        },
        {
          name: 'Y3'
        },
        {
          name: 'Y4'
        },
        {
          name: 'Y5'
        }
      ],
      selectedValues: [],
      name: 'columns'
    }
  };

  filtersQueryParams: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.setFilters();
  }

  setFilters(): void {
    const route = this.route.snapshot;
    this.filtersQueryParams = JSON.parse(JSON.stringify(route.queryParams));
    if (typeof this.filtersQueryParams.columns === 'string') {
      this.filtersQueryParams.columns = [this.filtersQueryParams.columns];
    }
    if (this.filtersQueryParams.columns) {
      this.filtersTemplate.tableColumns.selectedValues = this.filtersQueryParams.columns.map(item => ({name: item}));
    }
  }

  applyFilters(): void {
    this.filtersQueryParams = {};
    Object.keys(this.filtersTemplate).forEach(item => {
      const el = this.filtersTemplate[item];
      if (el.selectedValues) {
        this.filtersQueryParams[el.name] = el.selectedValues.map(_item => _item.name);
      }
    });
    this.router.navigate(['/', this.route.snapshot.routeConfig.path], {
        queryParams: this.filtersQueryParams.columns.length ? this.filtersQueryParams : undefined,
        fragment: 'table'
      }).then(() => this.filtersChanged.emit());
  }

  clearModel(): void {
      this.filtersTemplate.tableColumns.selectedValues = [];

      if (this.filtersQueryParams.columns) {
        this.filtersQueryParams = {};
        const route = this.route.snapshot;
        this.router.navigate(['/', route.routeConfig.path], {fragment: 'table'})
                   .then(() => this.filtersChanged.emit());
      }
  }
}
