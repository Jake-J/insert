import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PieChartComponent } from './pie-chart.component';

const chartData = {
  name: 'Y1',
  values: [
      '0.999275846',
      '0.824858801',
      '0.304004652',
      '0.921498027',
      '0.776070889',
      '0.523913776',
      '0.906331608',
      '0.417367291',
      '0.723426979',
      '0.477686748',
      '0.652762058',
      '0.090059638'
  ]
};

describe('PieChartComponent', () => {
  let component: PieChartComponent;
  let fixture: ComponentFixture<PieChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PieChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PieChartComponent);
    component = fixture.componentInstance;
    component.chartData = chartData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
