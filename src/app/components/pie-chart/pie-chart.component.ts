import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';

import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
  ChartComponent,
  ApexLegend,
  ApexPlotOptions,
  ApexDataLabels
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  legend: ApexLegend,
  plotOptions: ApexPlotOptions,
  dataLabels: ApexDataLabels,
  labels: any;
  fill: any;
};
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PieChartComponent implements OnInit {
  @ViewChild('chart') chart: ChartComponent;
  @Input() categories: any;
  @Input() chartData: any;
  public chartOptions: Partial<ChartOptions>;

  constructor() { }

  ngOnInit(): void {
    console.log(this.chartData);
    this.chartOptions = {
      series: this.chartData.values.map(val => isNaN(+val) ? 0 : +val),
      chart: {
        type: 'donut',
        height: 350
      },
      labels: this.categories,
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }
      ],
      fill: {
        colors: ['#52D726', '#FFEC00', '#FF7300', '#FF0000', '#007ED6', '#7CDDDD', '#7DA17D', '#F2D496', '#F1B11D', '#5B8F9A', '#394261', '#EC6B56']
      },
      legend: {
        offsetY: -5,
        labels: {
          useSeriesColors: false,
          colors: ['#43425D']
        },
        markers: {
          strokeWidth: 1,
          fillColors: ['#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff'],
          strokeColor: undefined
        },
        // if more time set const width by innerHTML for seriesName
        // document.querySelector('.apexcharts-legend-text').innerHTML = '<span style="margin-left:15px">' + document.querySelector('.apexcharts-legend-text').innerText.split(' ')[0] + '</span>';
        formatter(seriesName, opts): string {
          return opts.w.globals.series[opts.seriesIndex] && opts.w.globals.series[opts.seriesIndex] > 0
                  ? `${seriesName} ${((opts.w.globals.series[opts.seriesIndex] / opts.w.globals.series.reduce((a, b) => a + b, 0)) * 100).toFixed(2)}%`
                  : 'Not counted value';
        }
      },
      dataLabels: {
        enabled: false,
      },
      plotOptions: {
        pie: {
          donut: {
            size: '60%',
            labels: {
              show: false,
              value: {
                show: false,
              },
              total: {
                show: true
              }
            }
          }
        }
      }
    };
  }
}
