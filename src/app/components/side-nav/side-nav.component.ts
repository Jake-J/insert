import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  sideNavTemplate: Array<{label: string, link: string}> = [
    {
      label: 'strona A',
      link: 'asite'
    },
    {
      label: 'strona B',
      link: 'bsite'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
