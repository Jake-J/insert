import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SiteComponent } from './views/site/site.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'asite',
    pathMatch: 'full',
  },
  {
    path: 'asite',
    component: SiteComponent,
    data: {
      type: 'A'
    }
  },
  {
    path: 'bsite',
    component: SiteComponent,
    data: {
      type: 'B'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {anchorScrolling: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
