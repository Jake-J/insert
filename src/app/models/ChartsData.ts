export class ChartsRecordTemplate {
    name: string;
    values: string[];
}

export class ChartsData {
    categories: string[];
    records: ChartsRecordTemplate[];
}
