import { ChartsData } from './ChartsData';
import { TableData } from './TableData';

export class SiteTemplate {
    chartsData: ChartsData;
    tableData: TableData;
    dataType: string;
}
