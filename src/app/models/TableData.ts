export class TableRecordTemplate {
    [key: string]: string;

}

export class TableData {
    headers: string[];
    originalHeaders: string[];
    records: TableRecordTemplate[];
}
