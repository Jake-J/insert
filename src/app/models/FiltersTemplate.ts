export class FilterTemplate {
    label: string;
    options: {name: string}[];
    selectedValues: {name: string}[];
    name: string;
}

export class FiltersTemplateList {
    [key: string]: FilterTemplate;
}
